# Калькулятор дробей

Калькулятор помогает выполнять над дробями или смешанными числами следующие математические операции:

- Сложение
- Вычитание
- Умножение
- Деление
- Сравнение

## Диаграмма классов

![](https://sun9-62.userapi.com/impg/4M9AngdPaFIBrbV2aCTa5t8JRlk8lzWcKoDDHw/GrMzenCOMY4.jpg?size=1036x568&quality=95&sign=81b7aed3bda62a3702dcac8c630236a5&type=album)

## Скриншоты

![](https://sun9-64.userapi.com/impg/wLpgyLG58TN_YRBPynLkZHDf5J4P0XPO-71_zQ/-_VkqHNaVXk.jpg?size=1128x543&quality=95&sign=d4cd051210bf26d2c633fcbd99c8a3bd&type=album)

![](https://sun7-23.userapi.com/impg/VHXmhRE5aKyFxV9aAsYg2e6Ob_atcYE1GK_gEg/2D66Avn7lP4.jpg?size=494x475&quality=95&sign=f8dc1957383faffd0fd152cb7efacb3c&type=album)

![](https://sun7-17.userapi.com/impg/jdHJYiK9jFfKqH-IrqXhGotLyFpuhc3bJ8WZGg/RiKStwQcjvY.jpg?size=1089x544&quality=95&sign=382bb80cfc1af5cd88a5e96824076470&type=album)

![](https://sun7-18.userapi.com/impg/kpMDTKWvQO9fWgMoeScm-Gr9Ucg_yTh_CGMw4w/noCRwhizK7U.jpg?size=916x445&quality=95&sign=464a28be51ad600712bda35d28b5b58a&type=album)

![](https://sun7-24.userapi.com/impg/eHNzpnjbRWXSaCjrlXGvksc_KcUnb0yv_UA83A/3mvd5uFKlx8.jpg?size=1119x541&quality=95&sign=17965b5295e1aa757159b8417e575df6&type=album)

![](https://sun9-3.userapi.com/impg/-hw-eaNBP_5kor1QIGk5k6jYGLcScSexByObPw/-udOsRscnsI.jpg?size=597x483&quality=95&sign=4661cf6052242c04ea0ef857e39891d8&type=album)

Дополнительно реализован режим расчёта пропорций.

![](https://sun9-53.userapi.com/impg/nxFDBcopdPl3YUcPwprt-UVOKTWkxqIkwTUdxg/6kVA8KSuLl8.jpg?size=1040x540&quality=95&sign=07ccf836b2f768e7a36ed51398bf19ff&type=album)

![](https://sun9-7.userapi.com/impg/TQSc_U76zxU8RJhUJK3uJjn0cIChtATC_IJlXg/rulq_ZW1eFc.jpg?size=381x457&quality=95&sign=2c776d445e41fb69a8aa18d923959068&type=album)