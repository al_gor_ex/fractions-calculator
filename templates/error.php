<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ошибка</title>
    <link rel="stylesheet" href="assets/css/controls.css">
    <link rel="stylesheet" href="assets/css/error.css">
</head>
<body>
    <div class="wrapper">
        <div class="error">
            <?= $error->getMessage() ?>
        </div>
        <div>
            <a href="index.html" autofocus class="dark-button">
                ⤺
            </a>
        </div>        
    </div>
</body>
</html>