<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Результат</title>
    <link rel="stylesheet" href="assets/css/controls.css">
    <link rel="stylesheet" href="assets/css/result.css">
</head>
<body>
    <div class="wrapper">
        <?php if ($_REQUEST['operation'] === 'proportion'): ?>
            <div class="text-result">
                <?= $result ?>
            </div>
        <?php else: ?>
            <?php if ($result instanceof App\Models\Enums\ComparisonResult):?>
                <div class="text-result">
                    левая дробь <?= $result->value ?> правой дроби
                </div>
            <?php else: ?>
                <div class="fractional-result">
                    <?php if ($result->isNegative):?>
                        <div class="sign">
                            ―
                        </div>
                    <?php endif; ?>
                    <?php if($result->wholePart != 0 || $result->fractionalPart->numerator == 0): ?>
                        <div class="whole-part">
                            <?= $result->wholePart ?>
                        </div>
                    <?php endif; ?>
                    <?php if($result->fractionalPart->numerator != 0): ?>
                        <div class="fractional-part">
                            <div class="numerator">
                                <?= $result->fractionalPart->numerator ?>
                            </div>
                            <div>
                                <?= $result->fractionalPart->denominator ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <a href="index.html" autofocus class="dark-button btn-go-back">
            ⤺
        </a>        
    </div>
</body>
</html>