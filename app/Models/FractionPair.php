<?php

namespace App\Models;

class FractionPair
{
    public function __construct(
        public Fraction $frac1,
        public Fraction $frac2
    )
    {
    }
}