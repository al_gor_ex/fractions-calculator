<?php

namespace App\Models;

use App\Models\Enums\Operator;

class Expression
{
    public function __construct(
        public MixedNumber $leftNumber,
        public Operator $operator,
        public MixedNumber $rightNumber
    )
    {
    }
}