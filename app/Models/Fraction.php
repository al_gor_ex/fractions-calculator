<?php

namespace App\Models;

class Fraction
{
    public function __construct(
        public int $numerator = 0,
        public int $denominator = 1
    )
    {
    }
}