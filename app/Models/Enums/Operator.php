<?php

namespace App\Models\Enums;

enum Operator: string
{
    case Add = 'add';
    case Subtract = 'subtract';
    case Multiply = 'multiply';
    case Divide = 'divide';
    case Compare = 'compare';
}