<?php

namespace App\Models\Enums;

enum ComparisonResult: string
{
    case Equal = '=';
    case Greater = '>';
    case Less = '<';
}