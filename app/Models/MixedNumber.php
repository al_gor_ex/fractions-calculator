<?php

namespace App\Models;

class MixedNumber
{
    public function __construct(
        public Fraction $fractionalPart,
        public int $wholePart = 0,
        public bool $isNegative = false
    )
    {
        
    }
}