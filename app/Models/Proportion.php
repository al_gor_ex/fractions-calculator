<?php

namespace App\Models;

class Proportion
{
    public function __construct(
        public Fraction $leftFraction,
        public Fraction $rightFraction
    )
    {
    }
}