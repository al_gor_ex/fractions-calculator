<?php

namespace App\Algorithms\Operations;

use App\Models\Enums\ComparisonResult;
use App\Models\Fraction;
use App\Models\FractionPair;

abstract class Operation
{
    abstract public static function getResult(FractionPair $args): Fraction | ComparisonResult; 
}