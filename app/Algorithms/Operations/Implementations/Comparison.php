<?php

namespace App\Algorithms\Operations\Implementations;

use App\Algorithms\Operations\Operation;
use App\Models\FractionPair;
use App\Algorithms\FractionsReducer;
use App\Models\Enums\ComparisonResult;

class Comparison extends Operation 
{
    public static function getResult(FractionPair $args): ComparisonResult
    {
        $args = FractionsReducer::bringToCommonDenominator($args);
        return match ($args->frac1->numerator <=> $args->frac2->numerator) {
            1 => ComparisonResult::Greater,
            0 => ComparisonResult::Equal,
            -1 => ComparisonResult::Less
        };
    }
}