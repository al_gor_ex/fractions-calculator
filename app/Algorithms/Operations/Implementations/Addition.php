<?php

namespace App\Algorithms\Operations\Implementations;

use App\Algorithms\FractionsReducer;
use App\Algorithms\Operations\Operation;
use App\Models\FractionPair;
use App\Models\Fraction;

class Addition extends Operation
{
    public static function getResult(FractionPair $args): Fraction
    {
        $args = FractionsReducer::bringToCommonDenominator($args);
        return new Fraction(
            $args->frac1->numerator + $args->frac2->numerator,
            $args->frac1->denominator
        );
    }
}