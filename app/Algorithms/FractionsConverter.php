<?php

namespace App\Algorithms;

use App\Models\Fraction;
use App\Models\MixedNumber;

class FractionsConverter
{
    public static function getImproperFraction(MixedNumber $number): Fraction
    {
        $result = clone $number->fractionalPart;
        $result->numerator += $number->wholePart * $number->fractionalPart->denominator;
        if ($number->isNegative) {
            $result->numerator *= -1;
        }        
        return $result;
    }

    public static function getMixedNumber(Fraction $fraction): MixedNumber
    {
        $result = static::separateWholePart($fraction);
        $result->fractionalPart = static::simplify($result->fractionalPart);
        return $result;
    }

    private static function separateWholePart(Fraction $fraction): MixedNumber
    {
        return new MixedNumber(
            new Fraction(
                abs($fraction->numerator) % $fraction->denominator,
                $fraction->denominator
            ),
            intdiv(abs($fraction->numerator), $fraction->denominator),
            $fraction->numerator < 0
        );
    }

    private static function simplify(Fraction $fraction): Fraction
    {
        $gcd = CommonsCalculator::getGreatestCommonDivisor($fraction->numerator, $fraction->denominator);
        $result = clone $fraction;
        $result->numerator /= $gcd;
        $result->denominator /= $gcd;
        return $result;
    }
}