<?php

namespace App\Algorithms;

use App\Models\Fraction;
use App\Models\FractionPair;

class FractionsReducer
{
    public static function bringToCommonDenominator(FractionPair $fractions): FractionPair
    {
        $commonDenominator = static::getCommonDenominator($fractions);
        $result = clone $fractions;
        foreach ([$result->frac1, $result->frac2] as &$fraction) {
            $additionalFactor = static::getAdditionalFactor($fraction, $commonDenominator);
            $fraction->numerator *= $additionalFactor;
            $fraction->denominator *= $additionalFactor;
        }
        unset($fraction);
        return $result;
    }

    private static function getCommonDenominator(FractionPair $fractions): int
    {
        return CommonsCalculator::getLeastCommonMultiple(
            $fractions->frac1->denominator,
            $fractions->frac2->denominator
        );
    }

    private static function getAdditionalFactor(Fraction $fraction, int $commonDenominator): int
    {
        return $commonDenominator / $fraction->denominator;
    }
}