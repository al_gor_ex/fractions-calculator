<?php

namespace App\Algorithms;

class CommonsCalculator
{
    public static function getGreatestCommonDivisor(int $a, int $b): int
    {
        if ($a < $b) {
            static::swap($a, $b);
        }
        while ($b > 0) {
            $a %= $b;
            static::swap($a, $b);
        }
        return $a;
    }

    public static function getLeastCommonMultiple(int $a, int $b): int
    {
        return $a * $b / static::getGreatestCommonDivisor($a, $b);
    }

    private static function swap(int &$a, int &$b): void
    {
        $temp = $a;
        $a = $b;
        $b = $temp;
    }
}