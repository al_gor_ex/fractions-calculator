<?php

namespace App\Middleware;

class ConvertEmptyStringsToNull
{
    public static function process(array $request): array
    {
        foreach ($request as &$param) {
            if ($param === '') {
                $param = null;
            }
        }
        unset($param);
        return $request;
    }
}