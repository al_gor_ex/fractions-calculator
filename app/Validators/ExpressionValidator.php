<?php

namespace App\Validators;

use App\Exceptions\ValidationException;
use App\Models\Enums\Operator;
use ValueError;

class ExpressionValidator implements IValidator
{
    public static function validate(array $request): void
    {
        static::validateWholeParts($request);
        static::validateFractionsPresence($request);
        static::validateNumerators($request);
        static::validateDenominators($request);
        static::validateOperator($request);
    }

    private static function validateWholeParts(array $request): void
    {
        foreach (['leftInteger', 'rightInteger'] as $paramName) {
            if (isset($request[$paramName]) &&
                filter_var($request[$paramName], FILTER_VALIDATE_INT) === false) {
                throw new ValidationException('Недопустимое значение целой части');
            }
        }
    }

    private static function validateFractionsPresence(array $request): void
    {
        foreach (['left', 'right'] as $prefix) {
            if (isset($request[$prefix . 'Numerator']) && !isset($request[$prefix . 'Denominator']) ||
            !isset($request[$prefix . 'Numerator']) && isset($request[$prefix . 'Denominator'])) {
                throw new ValidationException('Числитель и знаменатель должны присутствовать вместе либо отсутствовать');
            }
        }
    }

    private static function validateNumerators(array $request): void
    {
        foreach (['left', 'right'] as $prefix) {
            if (isset($request[$prefix . 'Numerator']) &&
                (filter_var($request[$prefix . 'Numerator'], FILTER_VALIDATE_INT) === false ||
                $request[$prefix . 'Numerator'] < 1)) {
                throw new ValidationException('Числитель должен являться целым числом, большим 0');
            }
        }
    }

    private static function validateDenominators(array $request): void
    {
        foreach (['left', 'right'] as $prefix) {
            if (isset($request[$prefix . 'Denominator']) &&
                (filter_var($request[$prefix . 'Denominator'], FILTER_VALIDATE_INT) === false ||
                $request[$prefix . 'Denominator'] < 2)) {
                throw new ValidationException('Знаменатель должен являться целым числом, большим 1');
            }
        }
    }

    private static function validateOperator(array $request): void
    {
        try {
            Operator::from($request['operation']);
        } catch (ValueError) {
            throw new ValidationException('Неверный тип операции');
        }
    }
}
