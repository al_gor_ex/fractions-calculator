<?php

namespace App\Validators;

interface IValidator
{
    public static function validate(array $request): void;
}