<?php

namespace App\Validators;

use App\Validators\IValidator;
use App\Exceptions\ValidationException;

class ProportionValidator implements IValidator
{
    public static function validate(array $request): void
    {
        static::validateValues($request);
    }

    private static function validateValues(array $request): void
    {
        foreach (['leftNumerator', 'leftDenominator', 'rightNumerator'] as $paramName) {
            if (!isset($request[$paramName]) ||
                filter_var($request[$paramName], FILTER_VALIDATE_INT) === false ||
                $request[$paramName] < 1) {
                throw new ValidationException('Недопустимые входные значения');
            }
        }
    }
}