<?php

namespace App\Services;

use App\Algorithms\FractionsConverter;
use App\Algorithms\Operations\Implementations\Addition;
use App\Algorithms\Operations\Implementations\Comparison;
use App\Algorithms\Operations\Implementations\Division;
use App\Algorithms\Operations\Implementations\Multiplication;
use App\Algorithms\Operations\Implementations\Subtraction;
use App\Models\Enums\ComparisonResult;
use App\Models\Enums\Operator;
use App\Models\Expression;
use App\Models\Fraction;
use App\Models\FractionPair;
use App\Models\MixedNumber;
use App\Models\Proportion;

class SolutionService
{
    /**
     * @var array<string, Operation>
     */
    private static array $operatorToOperationClassMap;

    public static function solveExpression(Expression $expression): MixedNumber | ComparisonResult
    {
        $args = new FractionPair(
            FractionsConverter::getImproperFraction($expression->leftNumber),
            FractionsConverter::getImproperFraction($expression->rightNumber)
        );
        static::initOperationsMap();
        $result = static::$operatorToOperationClassMap[$expression->operator->value]::getResult($args);
        if ($result instanceof Fraction) {
            $result = FractionsConverter::getMixedNumber($result);
        }
        return $result;
    }

    public static function solveProportion(Proportion $proportion): int | float
    {
        $l = $proportion->leftFraction;
        $r = $proportion->rightFraction;
        return ($l->denominator * $r->numerator / $l->numerator);
    }

    private static function initOperationsMap(): void
    {
        static::$operatorToOperationClassMap = [
            Operator::Add->value => Addition::class,
            Operator::Subtract->value => Subtraction::class,
            Operator::Multiply->value => Multiplication::class,
            Operator::Divide->value => Division::class,
            Operator::Compare->value => Comparison::class
        ];
    }
}