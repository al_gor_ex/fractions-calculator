<?php

use App\Exceptions\ValidationException;
use App\Middleware\ConvertEmptyStringsToNull;
use App\Models\Enums\Operator;
use App\Models\Expression;
use App\Models\Fraction;
use App\Models\MixedNumber;
use App\Models\Proportion;
use App\Services\SolutionService;
use App\Validators\ExpressionValidator;
use App\Validators\ProportionValidator;

require_once __DIR__ . '/vendor/autoload.php';

$request = ConvertEmptyStringsToNull::process($_POST);

try {
    if (!isset($request['operation'])) {
        throw new ValidationException('Не выбрана операция');
    }
    if ($request['operation'] == 'proportion') {
        ProportionValidator::validate($request);
        $proportion = new Proportion(
            new Fraction(
                $request['leftNumerator'],
                $request['leftDenominator']
            ),
            new Fraction(
                $request['rightNumerator']
            )
        );
        $result = SolutionService::solveProportion($proportion);
    } else {
        ExpressionValidator::validate($request);
        $expression = new Expression(
            new MixedNumber(
                new Fraction(
                    $request['leftNumerator'] ?? 0,
                    $request['leftDenominator'] ?? 1
                ),
                $request['leftWholePart'] ?? 0,
                isset($request['isLeftNegative'])
            ),
            Operator::from($request['operation']),
            new MixedNumber(
                new Fraction(
                    $request['rightNumerator'] ?? 0,
                    $request['rightDenominator'] ?? 1
                ),
                $request['rightWholePart'] ?? 0,
                isset($request['isRightNegative'])
            )
        );
        $result = SolutionService::solveExpression($expression);
    }
    require_once __DIR__ . '/templates/result.php';
} catch (Throwable $error) {
    require_once __DIR__ . '/templates/error.php';
}
