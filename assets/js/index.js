function onSignCheckboxChange(event) {
    let signArea = event.target.parentElement
    let minusLabel = signArea.querySelector('div[class^="minus-label"]')
    minusLabel.classList.toggle('minus-label--enabled')
    minusLabel.classList.toggle('minus-label--disabled')
}

function onOperationSelect(event) {
    let signCheckboxes = document.querySelectorAll('input[name$="Negative"]')
    let minusLabels = document.querySelectorAll('div[class^="minus-label"]')
    let wholeParts = document.querySelectorAll('input[name$="WholePart"]')
    let rightDenominator = document.querySelector('input[name="rightDenominator"]')
    if (event.target.value === 'proportion') {
        for (let checkbox of signCheckboxes) {
            checkbox.disabled = true
            checkbox.classList.add('hidden')
        }
        for (let label of minusLabels) {
            label.classList.add('hidden')
        }
        for (let wholePart of wholeParts) {
            wholePart.classList.add('hidden')
            wholePart.disabled = true
        }
        rightDenominator.disabled = true
        rightDenominator.type = 'text'
        rightDenominator.value = 'X'
    } else {
        for (let checkbox of signCheckboxes) {
            checkbox.disabled = false
            checkbox.classList.remove('hidden')
        }
        for (let label of minusLabels) {
            label.classList.remove('hidden')
        }
        for (let wholePart of wholeParts) {
            wholePart.classList.remove('hidden')
            wholePart.disabled = false
        }
        rightDenominator.disabled = false
        rightDenominator.type = 'number'
        rightDenominator.value = ''
    }
}

let signCheckboxes = document.querySelectorAll('input[name$="Negative"]')
for (let checkbox of signCheckboxes) {
    checkbox.onchange = onSignCheckboxChange
}
let operationsList = document.forms.namedItem('expression').elements.namedItem('operation')
for (let operationOption of operationsList) {
    operationOption.onchange = onOperationSelect
}