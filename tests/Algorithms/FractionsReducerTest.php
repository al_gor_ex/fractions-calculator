<?php

use App\Algorithms\FractionsReducer;
use App\Models\Fraction;
use App\Models\FractionPair;
use PHPUnit\Framework\TestCase;

class FractionsReducerTest extends TestCase
{
    public static function fractionPairProvider(): array
    {
        return [
            [
                new FractionPair(new Fraction(5, 6), new Fraction(3, 8)),
                new FractionPair(new Fraction(20, 24), new Fraction(9, 24))
            ],
            [
                new FractionPair(new Fraction(-1, 2), new Fraction(-5, 8)),
                new FractionPair(new Fraction(-4, 8), new Fraction(-5, 8))
            ]
        ];
    }

    /**
     * @dataProvider fractionPairProvider
     */
    public function testBringToCommonDenominator(FractionPair $input, FractionPair $expected)
    {
        $actual = FractionsReducer::bringToCommonDenominator($input);
        static::assertTrue(
            $expected->frac1->numerator == $actual->frac1->numerator &&
            $expected->frac1->denominator == $actual->frac1->denominator &&
            $expected->frac2->numerator == $actual->frac2->numerator &&
            $expected->frac2->denominator == $actual->frac2->denominator
        );
    }
}