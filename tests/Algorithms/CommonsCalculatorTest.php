<?php

use App\Algorithms\CommonsCalculator;
use PHPUnit\Framework\TestCase;

class CommonsCalculatorTest extends TestCase
{
    public static function gcdProvider(): array
    {
        return [
            [32, 12, 4],
            [72, 128, 8]
        ];
    }

    public static function lcmProvider(): array
    {
        return [
            [28, 21, 84],
            [270, 315, 1890]
        ];
    }

    /**
     * @dataProvider gcdProvider
     */
    public function testGcd(int $a, int $b, int $expected)
    {
        $actual = CommonsCalculator::getGreatestCommonDivisor($a, $b);
        static::assertEquals($expected, $actual);
    }

    /**
     * @dataProvider lcmProvider
     */
    public function testLcm(int $a, int $b, int $expected)
    {
        $actual = CommonsCalculator::getLeastCommonMultiple($a, $b);
        static::assertEquals($expected, $actual);
    }
}