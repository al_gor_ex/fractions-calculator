<?php

use App\Algorithms\FractionsConverter;
use App\Models\Fraction;
use App\Models\MixedNumber;
use PHPUnit\Framework\TestCase;

class FractionsConverterTest extends TestCase
{
    public static function mixedNumbersProvider(): array
    {
        return [
            'wholePartZero' => [
                new MixedNumber(new Fraction(3, 4)),
                new Fraction(3, 4)
            ],
            'wholePartOnly' => [
                new MixedNumber(new Fraction(), 5),
                new Fraction(5, 1)
            ],
            'wholePartPositive' => [
                new MixedNumber(new Fraction(3, 4), 2),
                new Fraction(11, 4)
            ],
            'wholePartNegative' => [
                new MixedNumber(new Fraction(3, 4), 2, true),
                new Fraction(-11, 4)
            ]               
        ];
    }

    public static function fractionsProvider(): array
    {
        return [
            'wholePartZero' => [
                new Fraction(6, 16),
                new MixedNumber(new Fraction(3, 8))
            ],
            'wholePartOnly' => [
                new Fraction(7, 1),
                new MixedNumber(new Fraction(), 7)
            ],
            'wholePartPositive' => [
                new Fraction(54, 24),
                new MixedNumber(new Fraction(1, 4), 2)
            ],
            'wholePartNegative' => [
                new Fraction(-54, 24),
                new MixedNumber(new Fraction(1, 4), 2, true)
            ],
            'onlyNegativeNumerator' => [
                new Fraction(-2, 4),
                new MixedNumber(new Fraction(1, 2), 0, true)
            ]         
        ];
    }

    /**
     * @dataProvider mixedNumbersProvider
     */
    public function testGetImproperFraction(MixedNumber $input, Fraction $expected)
    {
        $actual = FractionsConverter::getImproperFraction($input);
        static::assertTrue(
            $actual->numerator == $expected->numerator &&
            $actual->denominator == $expected->denominator
        );
    }

    /**
     * @dataProvider fractionsProvider
     */
    public function testGetMixedNumber(Fraction $input, MixedNumber $expected)
    {
        $actual = FractionsConverter::getMixedNumber($input);
        static::assertTrue(
            $actual->fractionalPart->numerator == $expected->fractionalPart->numerator &&
            $actual->fractionalPart->denominator == $expected->fractionalPart->denominator &&
            $actual->wholePart == $expected->wholePart,
            $actual->isNegative == $expected->isNegative
        );
    }
}