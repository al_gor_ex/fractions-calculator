<?php

use App\Exceptions\ValidationException;
use PHPUnit\Framework\TestCase;
use App\Validators\ProportionValidator;

class ProportionValidatorTest extends TestCase
{
    public static function proportionProvider(): array
    {
        return [
            'absent' => [['leftNumerator' => 1, 'leftDenominator' => 2]],
            'wrongType' => [['leftNumerator' => 'smth', 'leftDenominator' => 2, 'rightNumerator' => 3]],
            'negativeValue' => [['leftNumerator' => 1, 'leftDenominator' => -2, 'rightNumerator' => 3]]
        ];
    }

    /**
     * @dataProvider proportionProvider
     */
    public function testValidateValues(array $request)
    {
        static::expectException(ValidationException::class);
        ProportionValidator::validate($request);
    }
}