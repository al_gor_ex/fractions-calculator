<?php

use App\Exceptions\ValidationException;
use App\Validators\ExpressionValidator;
use PHPUnit\Framework\TestCase;

class ExpressionValidatorTest extends TestCase
{

    public static function wholePartsProvider(): array
    {
        return [
            'wrongTypeLeft' => [['leftInteger' => 'smth', 'rightInteger' => -1]],
            'wrongTypeRight' => [['leftInteger' => 5, 'rightInteger' => 0.007]]
        ];
    }

    public static function fractionsPresenceProvider(): array
    {
        return [
            'numeratorAbsence' => [['rightDenominator' => 5]],
            'denominatorAbsence' => [['leftNumerator' => 5]]
        ];
    }

    public static function fractionsProvider(): array
    {
        return [
            [['leftNumerator' => 0, 'leftDenominator' => 5]],
            [['leftNumerator' => 1, 'leftDenominator' => 0]],
            [['rightNumerator' => -0.75, 'rightDenominator' => 2]],
            [['rightNumerator' => 2, 'rightDenominator' => 1]]
        ];
    }

    /**
     * @dataProvider wholePartsProvider
     */
    public function testValidateWholeParts(array $request)
    {
        static::expectException(ValidationException::class);
        ExpressionValidator::validate($request);
    }

    /**
     * @dataProvider fractionsPresenceProvider
     */
    public function testValidateFractionsPresence(array $request)
    {
        static::expectException(ValidationException::class);
        ExpressionValidator::validate($request);
    }

    /**
     * @dataProvider fractionsProvider
     */
    public function testValidateFractions(array $request)
    {
        static::expectException(ValidationException::class);
        ExpressionValidator::validate($request);
    }
}
